#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include <inttypes.h>
#include <stdlib.h>

#define PIXEL_BYTES 3
#define BMP_BYTES_ALIGN 4

size_t get_padding_value(size_t img_width);
int32_t str_to_int32_t_(char* string);
int is_BMP_filename(char* filename);
int is_available_angle(int32_t angle);

#endif //IMAGE_TRANSFORMER_UTIL_H
