#ifndef IMAGE_TRANSFORMER_BMP_HANDLER_H
#define IMAGE_TRANSFORMER_BMP_HANDLER_H

#include "bmp_file.h"

#define BF_TYPE 0x4D42
#define BF_HEADSIZE 54
#define BF_RESERVED 0
#define B_OFFBITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BITCOUNT 24
#define BI_COMPRESSION 0
#define BI_XPELSPERMETER 0
#define BI_YPELSPERMETER 0
#define BI_CLRUSED 0
#define BI_CLRIMPORTANT 0

struct bmp_header get_new_header(struct image const* img);

#endif //IMAGE_TRANSFORMER_BMP_HANDLER_H
