#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image create_image(uint64_t img_width, uint64_t img_height);
void delete_image(struct image* del_image);

#endif //IMAGE_TRANSFORMER_IMAGE_H
