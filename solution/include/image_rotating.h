#ifndef IMAGE_TRANSFORMER_IMAGE_ROTATING_H
#define IMAGE_TRANSFORMER_IMAGE_ROTATING_H

#include "image.h"

enum available_angle {
    ANGLE_0 =  0,
    ANGLE_90 = 90,
    ANGLE_180 = 180,
    ANGLE_270 = 270,
    ANGLE_NEG90 = -90,
    ANGLE_NEG180 = -180,
    ANGLE_NEG270 = -270
};

struct image rotate(struct image const source, enum available_angle rotate_angle);

#endif //IMAGE_TRANSFORMER_IMAGE_ROTATING_H
