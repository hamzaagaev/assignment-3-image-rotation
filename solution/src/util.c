#include "util.h"
#include <string.h>

#define LEN_BMP_FORMAT 4
#define ASCII_CODE_ZERO 48
#define ASCII_CODE_NINE 57

size_t get_padding_value(size_t img_width) {
    size_t img_width_bytes = img_width * PIXEL_BYTES;
    return (BMP_BYTES_ALIGN - img_width_bytes % BMP_BYTES_ALIGN) % BMP_BYTES_ALIGN;
}

// я не знал о существовании atoi ¯\_(ツ)_/¯
int32_t str_to_int32_t_(char* string) {
    size_t len_of_str = strlen(string);
    int32_t result = 0;
    int is_negative = 0;
    size_t start_index = 0;
    if (string[0] == '-') {
        is_negative = 1;
        start_index = 1;
    }
    for (size_t i = start_index; i < len_of_str; i++) {
        if (string[i] >= ASCII_CODE_ZERO && string[i] <= ASCII_CODE_NINE) {
            result *= 10;
            result += string[i] - ASCII_CODE_ZERO;
        }
        else break;
    }
    if (is_negative) {
        result *= -1;
    }
    return result;
}

int is_BMP_filename(char* filename) {
    size_t len_of_str = strlen(filename);
    char* format_filename = filename + sizeof(char) * (len_of_str - LEN_BMP_FORMAT);
    int is_fit_format = !(strcmp(format_filename, ".bmp"));
    return is_fit_format;
}

int is_available_angle(int32_t angle) {
    switch (angle) {
        case 0:
        case 90:
        case 180:
        case 270:
        case -90:
        case -180:
        case -270:
            return 1;
        default:
            return 0;
    }
}
