#include "image_rotating.h"
#include <math.h>
#include <stdlib.h>

// принимает вектор размера 1х2 и угол
// возвращает перевернутый на угол вектор
static void rotating_vector(int64_t* vector, enum available_angle angle) {
    int64_t new_vector[2] = {0, 0};
    double angle_to_radian = (double)(angle) / 180 * M_PI;
    // матрица поворота
    double rotating_matrix[2][2] = {
            {cos(angle_to_radian), (-1 * sin(angle_to_radian))},
            {sin(angle_to_radian), cos(angle_to_radian)}
    };
    new_vector[0] = (int64_t)(rotating_matrix[0][0] * (double)vector[0]) +
           (int64_t)(rotating_matrix[0][1] * (double)vector[1]);
    new_vector[1] = (int64_t)(rotating_matrix[1][0] * (double)vector[0]) +
           (int64_t)(rotating_matrix[1][1] * (double)vector[1]);
    vector[0] = new_vector[0];
    vector[1] = new_vector[1];
}

// принимает источник изображения и угол поворота (0, 90, 180, 270, -90, -180, -270)
// возвращает перевернутое изображение (если удалось выделить память, иначе - ссылка на NULL в img.data)
struct image rotate(struct image const source, enum available_angle rotate_angle) {
//    поворот по часовой стрелке (при использовании матрицы поворота - против часовой)
    rotate_angle *= -1;
    uint64_t new_img_height = source.height;
    uint64_t new_img_width = source.width;
    if (abs(rotate_angle) % 180 == 90) {
        new_img_height = source.width;
        new_img_width = source.height;
    }
    struct image new_img = create_image(new_img_width, new_img_height);
    if (new_img.data == NULL)
        return new_img;
    size_t count_of_pixels = (size_t) new_img_height * new_img_width;
    int64_t coord_vector[2];
    if (rotate_angle == ANGLE_0) {
        for (size_t i = 0; i < count_of_pixels; i++)
            new_img.data[i] = source.data[i];
        return new_img;
    }
    for (size_t i = 0; i < count_of_pixels; i++) {
        coord_vector[0] = (int64_t)(i % source.width) - (int64_t)(source.width / 2);
        coord_vector[1] = (int64_t)(i / source.width) - (int64_t)(source.height / 2);

        rotating_vector(coord_vector, rotate_angle);
        size_t new_x = new_img_width / 2 + coord_vector[0];
        size_t new_y = new_img_height / 2 + coord_vector[1];

        // выравнивание координат изображения для случаев, когда из-за величины
        // ширины и/или высоты происходит неправильное нахождение координат точки
        if ((!(new_img_width % 2) && !(new_img_height % 2))) {
            switch (rotate_angle) {
                case ANGLE_NEG90:
                case ANGLE_270:
                    new_y--;
                    break;
                case ANGLE_NEG180:
                case ANGLE_180:
                    new_y--;
                    new_x--;
                    break;
                case ANGLE_NEG270:
                case ANGLE_90:
                    new_x--;
                default:
                    break;
            }
        }

        if (source.width % 2 && !(source.height % 2)) {
            switch (rotate_angle) {
                case ANGLE_NEG270:
                case ANGLE_90:
                    new_x--;
                    break;
                case ANGLE_NEG180:
                case ANGLE_180:
                    new_y--;
                    break;
                default:
                    break;
            }
        }

        size_t j = new_x + new_y * new_img_width;
        new_img.data[j] = source.data[i];
    }
    return new_img;
}
