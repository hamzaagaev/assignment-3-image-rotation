#include "image.h"
#include <stdio.h>
#include <stdlib.h>

// создает объект image и пытается выделить память для изображения
// не обрабатывает ошибку выделения памяти
struct image create_image(uint64_t img_width, uint64_t img_height) {
    struct image new_image = {0};// = malloc(sizeof(struct image));
    new_image.width = img_width;
    new_image.height = img_height;
    new_image.data = malloc(sizeof(struct pixel) * img_width * img_height);
    return new_image;
}

void delete_image(struct image* del_image) {
    if (del_image->data != NULL) {
        free(del_image->data);
        del_image->data = NULL;
    }
    del_image->width = 0;
    del_image->height = 0;
//    del_image = NULL;
}
