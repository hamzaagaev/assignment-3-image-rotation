#include "image.h"
#include "bmp_file.h"
#include "image_rotating.h"
#include "util.h"
#include <stdio.h>

#define COUNT_OF_ARGS 4

enum ERROR_CODES {
    ERROR_PARAMETERS = 1,
    ERROR_FILENAME,
    ERROR_ANGLE,
    ERROR_OPEN_FILE,
    ERROR_CLOSE_FILE,
    ERROR_READ_FILE,
    ERROR_WRITE_FILE,
    ERROR_MALLOC
};

int main(int argc, char** argv) {
//    (void) argc; (void) argv; // supress 'unused parameters' warning
    char* input_file_name = argv[1];
    char* output_file_name = argv[2];
    char* angle_str = argv[3];
    if (argc != COUNT_OF_ARGS) {
        fprintf(stderr, "There should be 3 parameters: <source-image>, <transformed-image>, <angle>.");
        return ERROR_PARAMETERS;
    }
    else if (!is_BMP_filename(input_file_name) || !is_BMP_filename(output_file_name)) {
        fprintf(stderr, "Input and output files should be in BMP format.");
        return ERROR_FILENAME;
    }
    int32_t angle = str_to_int32_t_(angle_str);
    if (!is_available_angle(angle)) {
        fprintf(stderr, "You should use one of available angles: [0, 90, 180, 270, -90, -180, -270].");
        return ERROR_ANGLE;
    }
    FILE* source_file = fopen(input_file_name, "rb");
    if (source_file == NULL) {
        fprintf(stderr, "Cannot open source file. Make sure your file exists.");
        return ERROR_OPEN_FILE;
    }
    struct image source_image = {0};
    enum read_status read_image_status = from_bmp(source_file, &source_image);
    if (read_image_status != READ_OK) {
        fprintf(stderr, "There is read error: %d.", read_image_status);
        return ERROR_READ_FILE;
    }
    int source_close = fclose(source_file);
    if (source_close) {
        fprintf(stderr, "Cannot close file.");
        return ERROR_CLOSE_FILE;
    }
    struct image transformed_image = rotate(source_image, angle);
    if (transformed_image.data == NULL) {
        fprintf(stderr, "No memory was allocated for rotated image.");
        return ERROR_MALLOC;
    }
    FILE* transformed_file = fopen(output_file_name, "wb");
    if (transformed_file == NULL) {
        fprintf(stderr, "Cannot open new file.");
        return ERROR_OPEN_FILE;
    }
    enum write_status write_image_status = to_bmp(transformed_file, &transformed_image);
    if (write_image_status != WRITE_OK) {
        fprintf(stderr, "There is write error: %d.", write_image_status);
        return ERROR_WRITE_FILE;
    }
    int transformed_close = fclose(transformed_file);
    if (transformed_close) {
        fprintf(stderr, "Cannot close file.");
        return ERROR_CLOSE_FILE;
    }
    delete_image(&source_image);
    delete_image(&transformed_image);
    return 0;
}
