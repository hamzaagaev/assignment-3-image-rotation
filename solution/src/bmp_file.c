#include "bmp_file.h"
#include "bmp_handler.h"
#include "util.h"

// считывает из входного файла in изображение, помещает его в *img
// возвращает код статуса считывания изображения
enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header bmp_head;
    size_t count_read = fread(&bmp_head, sizeof(struct bmp_header), 1, in);
    if (count_read == 0 && !feof(in)) {
        return READ_ERROR;
    }
    if (bmp_head.bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    } else if (bmp_head.biSize != BI_SIZE || bmp_head.biPlanes != BI_PLANES) {
        return READ_INVALID_HEADER;
    }
    delete_image(img);
    *img = create_image(bmp_head.biWidth, bmp_head.biHeight);
    if (img->data != NULL) {
        size_t padding = get_padding_value(bmp_head.biWidth);
        long offset = (long) bmp_head.bOffBits;
        size_t count_of_pixels = bmp_head.biWidth * bmp_head.biHeight;
        fseek(in, offset, SEEK_SET);
        for (size_t i = 0; i < count_of_pixels; i++) {
            uint8_t pixel_colors[PIXEL_BYTES];
            count_read = fread(pixel_colors, sizeof(uint8_t), PIXEL_BYTES, in);
            if (count_read == 0 && !feof(in)) {
                delete_image(img);
                return READ_ERROR;
            }
            img->data[i] = (struct pixel) {pixel_colors[0], pixel_colors[1], pixel_colors[2]};
            offset += PIXEL_BYTES;
            if ((i + 1) % bmp_head.biWidth == 0) {
                offset += (long) padding;
            }
            fseek(in, offset, SEEK_SET);
        }
        return READ_OK;
    } else {
        delete_image(img);
        return READ_IMAGE_NOT_ALLOCATED;
    }
}

// записывает в выходной поток файла out изображение
// овзвращает код статуса записи изображения
enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header bmp_head = get_new_header(img);
    size_t padding = get_padding_value(bmp_head.biWidth);
    long offset = (long) bmp_head.bOffBits;
    size_t count_of_pixels = bmp_head.biWidth * bmp_head.biHeight;
    size_t count_wrote = fwrite(&bmp_head, sizeof(struct bmp_header), 1, out);
    if (count_wrote != 1) {
        return WRITE_ERROR;
    }
    fseek(out, offset, SEEK_SET);
    for (size_t i = 0; i < count_of_pixels; i++) {
        uint8_t pixel_colors[PIXEL_BYTES] = {img->data[i].b, img->data[i].g, img->data[i].r};
        count_wrote = fwrite(pixel_colors, sizeof(uint8_t), PIXEL_BYTES, out);
        if (count_wrote != PIXEL_BYTES) {
            return WRITE_ERROR;
        }
        offset += PIXEL_BYTES;
        if ((i + 1) % bmp_head.biWidth == 0) {
            offset += (long) padding;
        }
        fseek(out, offset, SEEK_SET);
    }
    return WRITE_OK;
}
