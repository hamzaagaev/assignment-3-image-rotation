#include "bmp_handler.h"
#include "util.h"

struct bmp_header get_new_header(struct image const* img) {
    uint64_t img_width = img->width;
    uint64_t img_height = img->height;
    size_t padding = get_padding_value(img_width);
    struct bmp_header new_bmp_header = {0};
    new_bmp_header.bfType = BF_TYPE;
    new_bmp_header.bfileSize = BF_HEADSIZE + (img_width * PIXEL_BYTES + padding) * img_height;
    new_bmp_header.bfReserved = BF_RESERVED;
    new_bmp_header.bOffBits = B_OFFBITS;
    new_bmp_header.biSize = BI_SIZE;
    new_bmp_header.biWidth = img_width;
    new_bmp_header.biHeight = img_height;
    new_bmp_header.biPlanes = BI_PLANES;
    new_bmp_header.biBitCount = BI_BITCOUNT;
    new_bmp_header.biCompression = BI_COMPRESSION;
    new_bmp_header.biSizeImage = (img_width * PIXEL_BYTES + padding) * img_height;
    new_bmp_header.biXPelsPerMeter = BI_XPELSPERMETER;
    new_bmp_header.biYPelsPerMeter = BI_YPELSPERMETER;
    new_bmp_header.biClrUsed = BI_CLRUSED;
    new_bmp_header.biClrImportant = BI_CLRIMPORTANT;
    return new_bmp_header;
}
